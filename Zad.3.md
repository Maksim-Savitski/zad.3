# Ulubiona gra komputerowa - [Dota2](https://www.dota2.com/home)
![enter image description here](https://cdn.cloudflare.steamstatic.com/apps/dota2/images/dota2_social.jpg)



# O czym ta gra ?

Dota 2 polega na rozgrywaniu meczów w trybie wieloosobowym, w których dwie pięcioosobowe drużyny, wraz z jednostkami kontrolowanymi przez sztuczną inteligencję, próbują zniszczyć ufortyfikowane twierdze przeciwników. Każdy gracz wybiera i kontroluje postać zwaną „bohaterem”.

###Ta gra ma tylko jedną mape 

![enter image description here](https://www.researchgate.net/profile/Hiroyuki-Iida-4/publication/312617745/figure/fig2/AS:453863435182083@1485220883528/An-example-of-Dota2-map.png)

 Gracze są przypisywani do jednej z drużyn, wcielając się w członków jednej z dwóch frakcji: „Radiant” (pol. świetliści) i „Dire” – (pol. mroczni). Drużyna świetlistych rozpoczyna w południowo-zachodnim rogu mapy, natomiast mroczni po przeciwległej, północno-wschodniej stronie mapy.
Gracze mogą wybierać środkową śćieżkę albo easy i hard.
Każda drużyna ma nie więcej i nie mniej niż 5 bohaterów,czyli 5 vs 5.


##BOHATEROWIE

 Gracz ma do wyboru 122 bohaterów. Raz do roku dodają 2-4 nowych bohaterów.

![enter image description here](https://d.radikal.ru/d03/1905/78/f68524d3f7da.png)

Każdy bohater jest inny i trzeba ich wybierać w zależności od wyboru przeciwnika 

![enter image description here](https://pbs.twimg.com/media/EfsYR_zWoAIzTwF.jpg)

Bitwa pomiedzy drużynami trwa od 25 do 70 minut, natomiast trzeba zawczasu przemyśleć wybór swojego bohatera. 



###Umiejętności bohaterów 

Wszytkich umiejętności jest około 540
 ![enter image description here](https://www.teahub.io/photos/full/228-2286377_dota-2-arteezy-defense-of-the-ancients-portal.jpg)

Każdy bohater ma od 4 do 12 umiejętności. Na przykład Invoker ma 10 

![enter image description here](https://dota2ok.ru/wp-content/uploads/2017/05/Invoke_list.png)


## Przedmioty 
Przedmioty można kupić w czasie gry.


![enter image description here](https://bukmekerov.net/wp-content/uploads/2020/05/Dota-2-kak-kupit-prodat-i-obmenyat-predmety-na-torgovoj-ploshhadke-Steam-ili-na-storonnih-sajtah.jpg)
 Każdemu bohaterowi pasują różne przedmioty, gracz sam powinien zdecydować co ma wybrać, ze względu na to, że niektóre przedmioty kosztują bardzo dużo złota i jest limit: 6 przedmiotów maksymalnie. 

## Przebieg walki 


![enter image description here](https://f.rpp-noticias.io/2019/08/21/what-is-this-game-twitch-830838mp4_830839.png)

Każdy gracz przy pomocy bohatera, od czas od czasu rzuca umiejętnościami w innych bohaterów. Niektóre umiejętności mają czas oczekiwania 2:00 minuty, dlatego trzeba użyć ich w odpowiednim momencie.





##KTO WYGRYWA?

Wygrywa ta drużyna, która pierwsza da radę zniszczyć "tron" przeciwnikom.

![enter image description here](https://avatars.mds.yandex.net/get-zen_doc/2414075/pub_5ed349f22c27c9551b034ce3_5ed34e0ed708f125791b0c1b/scale_1200)

To jest najgłówniejszy budynek w gre, który po swojej stronie musisz bronić, a u przeciwników - zniszczyć. 


##Cyber sport 

Cyber zawodnicy Dota 2 są najbogatsi na świecie.

![enter image description here](https://static.mk.ru/upload/entities/2021/10/18/16/articles/detailPicture/db/20/91/d1/72000b7b89cc77395a2475c2184417d6.jpg)

 ostania walka w 2021 roku przyniosła rosyjskiej drużynie **18.5 millionów dolarów**



